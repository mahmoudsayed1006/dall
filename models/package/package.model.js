import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const PackageSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    month:{
        type:Number,
        required:true
    },
    cost:{
        type:String,
        required:true
    },
    description:{
        type:String,
        required:true
    },
    arabicDescription:{
        type:String,
        required:true,
        default:" "
    },
    defaultPackage:{
        type:Boolean,
        default:false
    },
    usersCount:{
        type:Number,
        default:0
    },
    deleted:{
        type:Boolean,
        default:false
    },

},{ timestamps: true });
PackageSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
PackageSchema.plugin(autoIncrement.plugin, { model: 'package', startAt: 1 });

export default mongoose.model('package', PackageSchema);