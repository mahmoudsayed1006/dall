import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';
import bcrypt from 'bcryptjs';
import isEmail from 'validator/lib/isEmail';
import { isImgUrl } from "../../helpers/CheckMethods";

const userSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    fullname: {
        type: String,
        required: true,
        trim: true
    },
    bio: {
        type: String,
        default:""
    },
    email: {
        type: String,
        trim: true,
       
    },
    phone: {
        type:String,
        required: true,
        trim: true,
    },
    nationalID: {
        type: Number,
    },
    img: {
        type: String,
        default:""
    },
    gallery: {
        type: [String],
        default:[]
    },
    password: {
        type: String,
        required: true
    },
    type: {
        type: String,
        enum: ['WORKER','ADMIN','USER'],
        required:true
    },
    city: {
        type: Number,
        ref:'city',
        required: true,
    },
    area: {
        type: Number,
        ref:'area',
        required: true,
    },
    category:{
        type: Number,
        ref: 'category'
    },
    subCategory:{
        type: Number,
        ref: 'category'
    },
    //package
    hasPackage:{
        type: Boolean,
        default: false
    },
    package:{
        type: Number,
        ref: 'package'
    },
    packageStart:{
        type: Date,
    },
    packageEnd:{
        type: Date,
    },
    //end package
     //ads package
    hasAdsPackage:{
        type: Boolean,
        default: false
    },
    adsPackage:{
        type: Number,
        ref: 'adsPackage'
    },
    adsPackageNumber:{
        type: Number,
        default:0
    },
    adsPackageStart:{
        type: Date,
    },
    adsPackageEnd:{
        type: Date,
    },
    //end package
    token:{
        type:[String],
    },
    location:{
        type:[Number]
    },
     //rate
    rateCount:{
        type:Number,
        default:0
    },
    rateNumbers:{
        type:Number,
        default:0
    },
    rate:{
        type:Number,
        default:0
    },
    
     //end rate
     
    block:{
        type: Boolean,
        default: false
    },
    active:{
        type: Boolean,
        default: false
    },
    verifycode: {
        type: Number
    },
    
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true, discriminatorKey: 'kind' });

userSchema.pre('save', function (next) {
    const account = this;
    if (!account.isModified('password')) return next();

    const salt = bcrypt.genSaltSync();
    bcrypt.hash(account.password, salt).then(hash => {
        account.password = hash;
        next();
    }).catch(err => console.log(err));
});

userSchema.methods.isValidPassword = function (newPassword, callback) {
    let user = this;
    bcrypt.compare(newPassword, user.password, function (err, isMatch) {
        if (err)
            return callback(err);
        callback(null, isMatch);
    });
};

userSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret.password;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
userSchema.plugin(autoIncrement.plugin, { model: 'user', startAt: 1 });
export default mongoose.model('user', userSchema);