import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
import { isImgUrl } from "../../helpers/CheckMethods";

const OrderSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    worker: {
        type: Number,
        ref: 'user',
    },
    client: {
        type: Number,
        ref: 'user',
    },
    category:{
        type:Number,
        ref:'category'
    },
    subCategory:{
        type: Number,
        ref: 'category'
    },
    offer:{
        type:Number,
        ref:'offer'
    },
    city:{
        type:Number,
        ref:'city'
    },
    area:{
        type:Number,
        ref:'area'
    },
    status:{
        type: String,
        enum: ['PENDING','CANCELED','CONFIRMED','COMPLETED'],
        required:true,
        default:'PENDING'
    },
    date:{
        type:String,
    },
    time:{
        type:String,
    },
    description:{
        type:String,
    },
    address:{
        type:String,
    },
    currentLocation: {
        type: [Number] 
    },
    Destination: {
        type: [Number] 
    },
    accept: {
        type: Boolean,
        default: false
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

OrderSchema.index({ location: '2dsphere' });
OrderSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        if (ret.destination) {
            ret.destination = ret.destination.coordinates;
        }
      
    }
});
autoIncrement.initialize(mongoose.connection);
OrderSchema.plugin(autoIncrement.plugin, { model: 'order', startAt: 1 });

export default mongoose.model('order', OrderSchema);