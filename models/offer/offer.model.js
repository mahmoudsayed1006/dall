import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';

const OfferSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    worker: {
        type: Number,
        ref: 'user',
        required:true
    },
    order:{
        type: Number,
        ref: 'order',
        required:true
    },
    client:{
        type: Number,
        ref: 'user',
    },
    
    cost:{
        type: String,
        //required:true
    },
    description:{
        type: String,
        //required:true
    },
    accept:{
        type:Boolean,
        default:false
    },
    lastOffer : {
        type : Boolean ,
        default : true
    },
    rejected: {
        type: Boolean,
        default: false
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

OfferSchema.index({ location: '2dsphere' });
OfferSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        if (ret.destination) {
            ret.destination = ret.destination.coordinates;
        }
    }
});
autoIncrement.initialize(mongoose.connection);
OfferSchema.plugin(autoIncrement.plugin, { model: 'offer', startAt: 1 });

export default mongoose.model('offer', OfferSchema);