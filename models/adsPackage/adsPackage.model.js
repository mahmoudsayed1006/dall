import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const adsPackageSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    month:{
        type:Number,
        required:true
    },
    adsNumber:{
        type:Number,
        required:true
    },
    cost:{
        type:String,
        required:true
    },
    type:{
        type:String,
        enum:["HIGH","MID","LOW"],
        default:'MID'
    },
    
    description:{
        type:String,
        required:true
    },
    arabicDescription:{
        type:String,
        required:true,
        default:" "
    },
    usersCount:{
        type:Number,
        default:0
    },
    defaultPackage:{
        type:Boolean,
        default:false
    },
    deleted:{
        type:Boolean,
        default:false
    },

},{ timestamps: true });
adsPackageSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
adsPackageSchema.plugin(autoIncrement.plugin, { model: 'adsPackage', startAt: 1 });

export default mongoose.model('adsPackage', adsPackageSchema);