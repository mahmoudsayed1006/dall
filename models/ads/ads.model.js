import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
import { isImgUrl } from "../../helpers/CheckMethods";

const AdsSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    description:{
        type:String,
        required:true
    },
    title:{
        type:String,
        required:true
    },
    img: {
        type: [String],
        required: true,
        validate: {
            validator: imgUrl => isImgUrl(imgUrl),
            message: 'img is invalid url'
        }
    },
    target:{
        type:String,
        ref:'category'
    },
    type: {
        type: String,
        enum: ['FREE','PREMIUM','DALL'],
        required:true
    },
    phone:{
        type:String,
    },
    email:{
        type:String,
    },
    link:{
        type:String,
    },
    user:{
        type:Number,
        ref:'user',
        required:true
    },
    area:{
        type:Number,
        ref:'area',
        required:true
    },
    city:{
        type:Number,
        ref:'city',
    },
    package:{
        type:Number,
        ref:'adsPackage',
    },
    packageType:{
        type:String,
        enum:["HIGH","MID","LOW"],
        default:'MID'
    },
    start:{
        type: Number,
    },
    end:{
        type: Number,
    },
    location:{
        type:[Number]
    },
    active:{
        type:Boolean,
        default:false
    },
    deleted:{
        type:Boolean,
        default:false
    },

},{ timestamps: true });
AdsSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
AdsSchema.plugin(autoIncrement.plugin, { model: 'ads', startAt: 1 });

export default mongoose.model('ads', AdsSchema);