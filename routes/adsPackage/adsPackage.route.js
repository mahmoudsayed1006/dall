import express from 'express';
import adsPackageController from '../../controllers/adsPackage/adsPackage.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        adsPackageController.validateBody(),
        adsPackageController.create
    )
    .get(adsPackageController.findAll);
 
router.route('/:adsPackageId')
    .put(
        requireAuth,
        adsPackageController.validateBody(true),
        adsPackageController.update
    )
    .get(adsPackageController.findById)
    .delete( requireAuth,adsPackageController.delete);


router.route('/:adsPackageId/beDefault')
    .put(
        requireAuth,
        adsPackageController.beDafault
    )

router.route('/:adsPackageId/beSmallest')
    .put(
        requireAuth,
        adsPackageController.beSmallest
    )
router.route('/:adsPackageId/beBiggest')
    .put(
        requireAuth,
        adsPackageController.beHight
    )
export default router;