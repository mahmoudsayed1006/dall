import express from 'express';
import AdsController from '../../controllers/ads/ads.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('ads').array('img',6),
        AdsController.validateBody(),
        AdsController.create
    )
    .get(AdsController.findAll);

router.route('/:AdsId')
    .put(
        requireAuth,
        multerSaveTo('Slider').fields([
            { name: 'img', maxCount: 6, options: false }
        ]),
        AdsController.validateBody(true),
        AdsController.update
    )
    .get(AdsController.findById)
    .delete( requireAuth,AdsController.delete);

router.route('/createAds')
    .post(
        requireAuth,
        multerSaveTo('ads').array('img',6),
        AdsController.validateBody(),
        AdsController.createAds
    )

router.route('/:AdsId/active')
    .put(
        requireAuth,
        AdsController.active
    )
export default router;