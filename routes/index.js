import express from 'express';
import userRoute from './user/user.route';
import ContactRoute  from './contact/contact.route';
import ReportRoute  from './reports/report.route';
import NotifRoute  from './notif/notif.route';
import AboutRoute  from './about/about.route';
import AdminRoute  from './admin/admin.route';
import OrderRoute  from './order/order.route';
import OfferRoute  from './offer/offer.route';
import AdsRoute  from './ads/ads.route';
import packageRoute  from './package/package.route';
import cityRoute  from './city/city.route';
import categoryRoute  from './category/category.route';
import AdsPackageRoute  from './adsPackage/adsPackage.route';
import { requireAuth } from '../services/passport';

const router = express.Router();

router.use('/', userRoute);
router.use('/contact-us',ContactRoute);
router.use('/reports',requireAuth, ReportRoute);
router.use('/notif',requireAuth, NotifRoute);
router.use('/admin',requireAuth, AdminRoute);
router.use('/about',AboutRoute);
router.use('/orders',OrderRoute);
router.use('/offers',OfferRoute);
router.use('/ads',AdsRoute);
router.use('/cities',cityRoute);
router.use('/package',packageRoute);
router.use('/adsPackage',AdsPackageRoute);
router.use('/categories',categoryRoute);

export default router;
