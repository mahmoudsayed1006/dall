import express from 'express';
import OrderController from '../../controllers/order/order.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .get(OrderController.findOrders)

router.route('/orderCount')
    .get(OrderController.countOrder)

router.route('/:orderId')
    //.put( requireAuth,OrderController.update) 
    .get(OrderController.findById)
    .delete( requireAuth,OrderController.delete);

router.route('/:orderId/cancel')
    .put( requireAuth,OrderController.cancel) 

router.route('/:orderId/confirm')
    .put( requireAuth,OrderController.confirm) 

router.route('/:orderId/finish')
    .put( requireAuth,OrderController.finish)

router.route('/:orderId/rate')
    .put( requireAuth,OrderController.ratValidateBody(),OrderController.rate)

router.route('/findAllRate/order')
    .get(OrderController.findAllRate)

router.route('/upload')
    .post( requireAuth,
        multerSaveTo('users').array('img',10),
        OrderController.uploadImage
    )  
    
export default router;
