import express from 'express';
import { requireSignIn, requireAuth } from '../../services/passport';
import UserController from '../../controllers/user/user.controller';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();


router.post('/signin',requireSignIn, UserController.signIn);

router.route('/signup')
    .post(
        multerSaveTo('users').single('img'),
        UserController.validateUserCreateBody(),
        UserController.signUp
    );
    router.route('/addWorker')
    .post(    
        multerSaveTo('users').single('img'),
        UserController.validateWorkerCreatedBody(),
        UserController.signUpWorker
    );

router.route('/find')
    .get(UserController.findAll);
    
router.route('/:id/getUser')
    .get(UserController.findById);

router.route('/:userId/delete')
    .delete(requireAuth,UserController.delete);

router.route('/:userId/block')
    .put(
        requireAuth,
        UserController.block
    );
router.route('/:userId/unblock')
    .put(
        requireAuth,
        UserController.unblock
    );
router.route('/:userId/active')
    .put(
        requireAuth,
        UserController.active
    );
router.route('/:userId/dis-active')
    .put(
        requireAuth,
        UserController.disactive
    );
router.route('/:userId/tracking')
    .put(
        requireAuth,
        UserController.tracking
    );

router.route('/logout')
    .post(
        requireAuth,
        UserController.logout
    );
router.route('/addToken')
    .post(
        requireAuth,
        UserController.addToken
    );
router.route('/updateToken')
    .put(
        requireAuth,
        UserController.updateToken
    );


router.put('/check-exist-email', UserController.checkExistEmail);

router.put('/check-exist-phone', UserController.checkExistPhone);

router.put('/user/:userId/updateInfo',
    requireAuth,
    multerSaveTo('users').single('img'),    
    UserController.validateUpdatedUser(true),
    UserController.updateUser);

router.put('/user/updatePassword',
    requireAuth,
    UserController.validateUpdatedPassword(),
    UserController.updatePassword);

router.post('/sendCode',
    UserController.validateSendCode(),
    UserController.sendCodeToEmail);

router.post('/confirm-code',
    UserController.validateConfirmVerifyCode(),
    UserController.resetPasswordConfirmVerifyCode);

router.post('/reset-password',
    UserController.validateResetPassword(),
    UserController.resetPassword);

router.post('/sendCode-phone',
    UserController.validateForgetPassword(),
    UserController.forgetPasswordSms);

router.post('/confirm-code-phone',
    UserController.validateConfirmVerifyCodePhone(),
    UserController.resetPasswordConfirmVerifyCodePhone);

router.post('/reset-password-phone',
    UserController.validateResetPasswordPhone(),
    UserController.resetPasswordPhone);



router.put('/:userId/addToGallery',
    requireAuth,
    multerSaveTo('users').single('img'),
    UserController.addToGallery);

router.put('/:userId/removeFromGallery',
    requireAuth,
    UserController.removeFromGallery);

router.put('/:packageId/buyPackage',
    requireAuth,
    UserController.buyPackage);

router.put('/:packageId/buyAdsPackage',
    requireAuth,
    UserController.buyAdsPackage);

router.get('/findWorkersOfWeek',
    UserController.findWorkersOfWeek);

export default router;
