import { checkExist,checkCouponExist, checkExistThenGet, isLng, isLat} from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import Order from "../../models/order/order.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import { body, query } from "express-validator/check";
import { ValidationError } from "mongoose";
import { handleImg,handleImgs, checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import Notif from "../../models/notif/notif.model";
import Offer from "../../models/offer/offer.model";
import Rate from "../../models/rate/rate.model";

const populateQuery = [
    { path: 'client', model: 'user' },
    { path: 'worker', model: 'user' },
    { path: 'category', model: 'category' },
    { path: 'subCategory', model: 'category' },
    { path: 'offer', model: 'offer' },
    { path: 'area', model: 'area' },
    { path: 'city', model: 'city' },
    {
        path: 'worker', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'worker', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
    {
        path: 'worker', model: 'user',
        populate: { path: 'category', model: 'category' },
    },
    {
        path: 'worker', model: 'user',
        populate: { path: 'subCategory', model: 'category' },
    },
    {
        path: 'client', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'client', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
];
const populateQuery2 = [
    { path: 'user', model: 'user' },
    {
        path: 'worker', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'worker', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
    {
        path: 'worker', model: 'user',
        populate: { path: 'category', model: 'category' },
    },
    {
        path: 'worker', model: 'user',
        populate: { path: 'subCategory', model: 'category' },
    },
    
];


var OrderController = {
    async findOrders(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20
                ,{ client,worker,status,accept,category,subCategory,area} = req.query
                , query = {deleted: false};
           
            if (area)
                query.area = area;
            if (client)
                query.client = client;
            if (worker)
                query.worker = worker;
            if (status)
                query.status = status;
            if (category)
                query.category = category;
            if (subCategory)
                query.subCategory = subCategory;
            if (accept =='true')
                query.accept = true;
            let orders = await Order.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const ordersCount = await Order.count(query);
            const pageCount = Math.ceil(ordersCount / limit);

            res.send(new ApiResponse(orders, page, pageCount, limit, ordersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async countOrder(req,res, next) {
        try {
            let {worker,status,client,accept} = req.query
            , query = {deleted: false};
            if (area)
                query.area = area;
            if (worker)
                query.worker = worker;
            if (status)
                query.status = status;
            if (client)
                query.client = client;
            if (accept == 'true')
                query.accept = true;
            const orderCount = await Order.count(query);
            
            res.status(200).send({
                order:orderCount   
            });
        } catch (err) {
            next(err);
        }
        
    },
    
    async addOrder(socket,data,nsp){
        var orderData = { 
            category: data.category,
            subCategory: data.subCategory,
            client: data.client,
        }
        
        if (data.worker != null) {
            orderData.worker = data.worker;
        }
        if (data.currentLocation != null) {
            orderData.currentLocation = data.currentLocation;
        }
        if (data.destination != null) {
            orderData.Destination = data.destination;
        }
        if (data.date != null) {
            orderData.date = data.date;
        }
        if (data.time != null) {
            orderData.time = data.time;
        }
        if (data.description != null) {
            orderData.description = data.description;
        }
        if (data.address != null) {
            orderData.address = data.address;
        }
        if (data.city != null) {
            orderData.city = data.city;
        }
        if (data.area != null) {
            orderData.area = data.area;
        }
      
        let reports = {
            "action":"Create New Order",
        };

        let report = Report.create({...reports, user: data.client  });
       
        var order = new Order(orderData);   
        order.save()
        .then(async (data1) => {
            if(data.worker != null){
                let value =parseInt(data.worker);
                let query = {
                    _id:value,

                }
                if(data.city != null){
                    query.city = parseInt(data.city)
                }
                let users = await User.find(query);
                console.log(users)
                var query1 = {
                    _id: data1._id
                };
                
                users.forEach(user => {
                    var toRoom = 'room-' + user.id; 
                    Order.find(query1).populate(populateQuery)
                    .then(async (data) => {
                        nsp.to(toRoom).emit('newOrder', {order:data});
                    });
                })
                User.find(query)
                .then((data) => {
                    data.map(function (element) {
                        console.log(element)
                        sendNotifiAndPushNotifi({////////
                            targetUser: element.id, 
                            fromUser: data1.client, 
                            text: 'new notification',
                            subject: data1._id,
                            subjectType: 'new order'
                        });
                        let notif = {
                            "description":'new order',
                            "arabicDescription":"طلب جديد"
                        }
                        Notif.create({...notif,resource:data1.client,target:element.id,order:data1._id});
                    })
                
                })
                .catch(err => {
                    next(err);
                })
            } else{
                let value = parseInt(data.subCategory);
                let query3 = {
                    //_id:value,
                    type:"WORKER",
                    subCategory:value
                }
                if(data.city != null){
                    query3.city = parseInt(data.city)
                }
                let users = await User.find(query3);
                console.log(users)
                var query1 = {
                    _id: data1._id
                };
                
                users.forEach(user => {
                    var toRoom = 'room-' + user.id; 
                    Order.find(query1).populate(populateQuery)
                    .then(async (data) => {
                        nsp.to(toRoom).emit('newOrder', {order:data});
                    });
                })
                User.find(query3)
                .then((data) => {
                    data.map(function (element) {
                        console.log(element)
                        sendNotifiAndPushNotifi({////////
                            targetUser: element.id, 
                            fromUser: data1.client, 
                            text: 'new notification',
                            subject: data1._id,
                            subjectType: 'new order'
                        });
                        let notif = {
                            "description":'new order',
                            "arabicDescription":"طلب جديد"
                        }
                        Notif.create({...notif,resource:data1.client,target:element.id,order:data1._id});
                    })
                
                })
                .catch(err => {
                    next(err);
                })

                }
            
          
        })
        .catch((err)=>{
            console.log(err)
        });
    },
    async uploadImage(req, res, next) {
        try {
            let image = await handleImgs(req);
            console.log(req.file)
            console.log(image)
            
            res.send(image);
            } catch (error) {
            next(error)
        }
    },
    async cancelSocket(nsp, data) { 

        var toId = data.toId;
        var fromId = data.fromId;
        var orderId = data.orderId;
        var toRoom = 'room-' + data.toId;
        console.log(data.toId)
        console.log(data.fromId)
        console.log(data.orderId)
        
        var query1 = {
            _id: data.orderId
        };
        Order.updateMany(query1, { status: 'CANCELED'})
            .exec()
            .then(async(result) => {
                Order.find(query1).populate(populateQuery)
                .then(async (data) => {
                    nsp.to(toRoom).emit('cancel', {order:data});
                    sendNotifiAndPushNotifi({////////
                        targetUser: data.toId, 
                        fromUser: data.fromId, 
                        text: 'new notification',
                        subject: data.orderId,
                        subjectType: 'order has been cancelled'
                    });
                    let notif = {
                        "description":'order has been cancelled',
                        "arabicDescription":"تم الغاء الطلب"
                    }
                    Notif.create({...notif,resource:data.fromId,target:data.toId,order:data.orderId});
                    let reports = {
                        "action":"client cancel the order",
                    };
                    let report = await Report.create({...reports, user: fromId});
                });
               
                
            })
            .catch((err) => {
                console.log(err);
            });
    }, 
    async confirmSocket(nsp, data) { 

        var clientId = data.clientId;
        var workerId = data.workerId;
        var orderId = data.orderId;
        var toRoom = 'room-' + data.clientId;
        console.log(toRoom)
        
        var query1 = {
            _id: data.orderId
        };
        Order.updateMany(query1, { status: 'CONFIRMED'})
            .exec()
            .then(async(result) => {
                Order.find(query1).populate(populateQuery)
                .then(async (data) => {
                    nsp.to(toRoom).emit('confirm', {order:data});
                    sendNotifiAndPushNotifi({////////
                        targetUser: data.clientId, 
                        fromUser: workerId, 
                        text: 'new notification',
                        subject: orderId,
                        subjectType: 'order has been confirmed'
                    });
                    let notif = {
                        "description":'order has been confirmed',
                        "arabicDescription":"تم تأكيدالطلب"
                    }
                    Notif.create({...notif,resource:workerId,target:data.clientId,order:data.orderId});
                    let reports = {
                        "action":"worker confirm order",
                    };
                    let report = await Report.create({...reports, user: data.clientId});
                });
            })
            .catch((err) => {
                console.log(err);
            });
    },
    
    
    async cancel(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.status = 'CANCELED';
            await order.save();
            let to ;
            if(req.user.type == "WORKER"){
                to = order.client
            }else{
                to = order.worker
            }
            sendNotifiAndPushNotifi({////////
                targetUser: to, 
                fromUser: req.user._id, 
                text: 'new notification',
                subject: order.id,
                subjectType: 'order has been cancelled'
            });
            let notif = {
                "description":'order has been cancelled',
                "arabicDescription":"تم الغاء الطلب"
            }
            Notif.create({...notif,resource:req.user._id,target:to,order:order.id});
            let reports = {
                "action":"client cancel the order",
            };
            let report = await Report.create({...reports, user: req.user});
            res.send('client cancel order');
            
        } catch (error) {
            next(error);
        }
    },
    async confirm(req, res, next) {
        try {
            let { orderId } = req.params;
            
            let order = await checkExistThenGet(orderId, Order);
            order.status = 'CONFIRMED';
            await order.save();
            sendNotifiAndPushNotifi({////////
                targetUser: order.client, 
                fromUser: order.worker, 
                text: 'new notification',
                subject: order.id,
                subjectType: 'order has been confirmed'
            });
            let notif = {
                "description":'order has been confirmed',
                "arabicDescription":"تم تأكيدالطلب"
            }
            Notif.create({...notif,resource:order.worker,target:order.client,order:order.id});
            let reports = {
                "action":"worker confirm order",
            };
            let report = await Report.create({...reports, user: req.user});
            res.send('worker confirm order');
            
        } catch (error) {
            next(error);
        }
    },
    async finish(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.status = 'COMPLETED';
            order.accept = true;
            await order.save();
            sendNotifiAndPushNotifi({////////
                targetUser: order.worker, 
                fromUser: order.client, 
                text: 'new notification',
                subject: order.id,
                subjectType: 'order has been completed'
            });
            let notif = {
                "description":'order has been completed',
                "arabicDescription":"تم انجاز الطلب"
            }
            Notif.create({...notif,resource:order.client,target:order.worker,order:order.id});
           
            let reports = {
                "action":"order Finished",
            };
            let report = await Report.create({...reports, user: req.user});
            res.send('order Finished');
            
        } catch (error) {
            next(error);
        }
    },
    async findById(req, res, next) {
        try {
            let {orderId } = req.params;
            res.send(
                await checkExistThenGet(orderId, Order, { deleted: false, populate: populateQuery })
            );
        } catch (err) {
            next(err);
        }
    },
    
    async delete(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.deleted = true;
            await order.save();
            
            let reports = {
                "action":"Delete Order",
            };
            let report = await Report.create({...reports, user: req.user});
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    ratValidateBody(isUpdate = false) {
        let validations = [
            body('comment').not().isEmpty().withMessage('comment is required'),
            body('rate').not().isEmpty().withMessage('rate is required')
            .isNumeric().withMessage('rate numeric value required'),
            
        ];
        return validations;
    },
    async rate(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order, { deleted: false });
            const validatedBody = checkValidations(req);
            validatedBody.order = orderId;
            validatedBody.user = req.user._id;
            validatedBody.worker = order.worker;
            let d = new Date();
            let dateMilleSec = Date.parse(d)
            validatedBody.date = dateMilleSec;
            await Rate.create({ ...validatedBody });

             //rate
            let user = await checkExistThenGet(order.worker, User, { deleted: false });
            let newRate = user.rateCount + parseInt(validatedBody.rate);
            user.rateCount = newRate;
            user.rateNumbers = user.rateNumbers + 1;
            let totalDegree = user.rateNumbers * 5; //عدد التاسكات فى الدرجه النهائيه
            let degree = newRate * 100
            let ratePrecent = degree / totalDegree;
            let rate = ratePrecent / 20
            console.log(ratePrecent)
            user.rate = Math.ceil(parseInt(rate));
            console.log( user.rate) 
            user.save();
            res.status(200).send("rate success")

        }
        catch (err) {
            next(err);
        }
    },
    async findAllRate(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let { orderId,worker } = req.query;
            let query = {deleted: false};
            if(orderId)
                query.order = orderId
            if(worker)
                query.worker = worker
            let orderRate = await Rate.find(query).populate(populateQuery2)
                .sort({createdAt: -1})
                .limit(limit)
                .skip((page - 1) * limit);


            const orderCount = await Rate.count(query);
            const pageCount = Math.ceil(orderCount / limit);

            res.send(new ApiResponse(orderRate, page, pageCount, limit, orderCount, req));
        } catch (err) {
            next(err);
        }
    },
   
};
module.exports = OrderController;