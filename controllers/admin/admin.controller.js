import ApiError from "../../helpers/ApiError";
import User from "../../models/user/user.model";
import Order from "../../models/order/order.model";
import Offer from "../../models/offer/offer.model";
import Contact from "../../models/contact/contact.model";
import Report from "../../models/reports/report.model";
import Area from "../../models/area/area.model";
import City from "../../models/city/city.model";
import Ads from "../../models/ads/ads.model";
import Package from "../../models/package/package.model";
import AdsPackage from "../../models/adsPackage/adsPackage.model";

import Category from "../../models/category/category.model";
import SubCategory from "../../models/category/sub-category.model";


const action = [
    { path: 'user', model: 'user' },
];
const populateQuery = [
    { path: 'client', model: 'user' },
    { path: 'worker', model: 'user' },
    { path: 'offer', model: 'offer' },

];
export default {
    async getLastUser(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {
              deleted: false
            };
         
            let lastUser = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },
    async getLastActions(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {deleted: false};
         
            let lastUser = await Report.find(query).populate(action)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },

    async getLastOrder(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let { status} = req.query
            let query = {deleted: false };
            if (status)
                query.status = status;                
            let lastOrder = await Order.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastOrder);
        } catch (error) {
            next(error);
        }
    },
   
    async count(req,res, next) {
        try {
            let query = { deleted: false };
            const usersCount = await User.count({deleted: false,type:'USER'});
            const workerCount = await User.count({deleted: false,type:'WORKER'});
            const pendingUsersCount = await User.count({deleted: false,active:true});
            const PendingOrdersCount = await Order.count({deleted:false,status:'PENDING'});
            const ONPROGRESSOrdersCount = await Order.count({deleted:false,status:'CONFIRMED'});
            const RefusedOrdersCount = await Order.count({deleted:false,status:'CANCEL'});
            const FINISHEDOrdersCount = await Order.count({deleted:false,status:'COMPLETED'});
            const messageCount = await Contact.count({deleted:false,reply:false});
            const cityCount = await City.count({deleted:false});
            const areaCount = await Area.count({deleted:false});
            const packagesCount = await Package.count({deleted:false});
            const AdsPackagesCount = await AdsPackage.count({deleted:false});
            const adsCount = await Ads.count({deleted:false});
            const categoriesCount = await Category.count({deleted:false});
            const subCategoriesCount = await SubCategory.count({deleted:false});
          
            res.status(200).send({
                users:usersCount,
                workers:workerCount,
                pendingUsersCount:pendingUsersCount,
                pending:PendingOrdersCount,
                onprogressOrders:ONPROGRESSOrdersCount,
                refused:RefusedOrdersCount,
                finished:FINISHEDOrdersCount,
                messages:messageCount,
                areaCount:areaCount,
                cityCount:cityCount,
                packagesCount:packagesCount,
                adsCount:adsCount,
                categoriesCount:categoriesCount,
                subCategoriesCount:subCategoriesCount,
                AdsPackagesCount:AdsPackagesCount
                
                
            });
        } catch (err) {
            next(err);
        }
        
    },
    
}