import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods';
import { body } from 'express-validator/check';
import { checkValidations, handleImg,handleImgs } from '../shared/shared.controller';
import { generateToken } from '../../utils/token';
import ApiResponse from "../../helpers/ApiResponse";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import Package from "../../models/package/package.model";
import Rate from "../../models/rate/rate.model";
import AdsPackage from "../../models/adsPackage/adsPackage.model";
import bcrypt from 'bcryptjs';
import config from '../../config';
import { toImgUrl } from "../../utils";
import { sendForgetPassword } from '../../services/message-service';
import { generateVerifyCode } from '../../services/generator-code-service';
import Notif from "../../models/notif/notif.model";
import { sendEmail } from "../../services/emailMessage.service";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import { sendMsg } from '../../services/sms-misr';
const checkUserExistByPhone = async (phone) => {
    let user = await User.findOne({ phone });
    if (!user)
        throw new ApiError.BadRequest('Phone Not Found');

    return user;
}
const checkUserExistByEmail = async (email) => {
    let user = await User.findOne({ email });
    if (!user)
        throw new ApiError.BadRequest('email Not Found');

    return user;
}
const populateQuery = [
    { path: 'city', model: 'city' },
    { path: 'area', model: 'area' },
    { path: 'package', model: 'package' },
    { path: 'adsPackage', model: 'adsPackage' },
    { path: 'category', model: 'category' },
    { path: 'subCategory', model: 'category' },
];
const populateQuery2 = [
    {
        path: 'worker', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'worker', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
    {
        path: 'worker', model: 'user',
        populate: { path: 'category', model: 'category' },
    },
    {
        path: 'worker', model: 'user',
        populate: { path: 'subCategory', model: 'category' },
    },
];
export default {
    async addToken(req,res,next){
        try{
            let user = req.user;
            let users = await checkExistThenGet(user.id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.token;
            });
            if(!found){
                users.token.push(req.body.token);
                await users.save();
            console.log(req.body.token);
            }
            res.status(200).send({
                users,
            });
            
        } catch(err){
            next(err);
        }
    },
    async signIn(req, res, next) {
        try{
            let user = req.user;
            user = await User.findById(user.id).populate(populateQuery);
           
            if(!user)
                return next(new ApiError(403, ('email or password incorrect')));
            
            if(user.block == true){
                return next(new ApiError(403, ('sorry you are blocked')));
            }
            if(user.deleted == true){
                return next(new ApiError(403, ('sorry you are deleted')));
            }
            /*if(user.active == false){
                return next(new ApiError(403, ('sorry you are not active')));
            }*/

            if(req.body.token != null && req.body.token !=""){
                let arr = user.token; 
                var found = arr.find(function(element) {
                    return element == req.body.token;
                });
                if(!found){
                    user.token.push(req.body.token);
                    await user.save();
                }
            }
           
            
            res.status(200).send({
                user,
                token: generateToken(user.id)
            });
            
        } catch(err){
            next(err);
        }
    },
    async socialLogin(req, res, next) {
        try{
            let user = await User.findOne({phone:req.body.phone}).populate(populateQuery);
            console.log(user)
            if(!user){
                user = await User.create({
                    fullname: req.body.fullname,
                    lastname: req.body.lastname,
                    email: req.body.email,
                    phone: req.body.phone,
                    type:req.body.type,
                    signUpFrom:req.body.signUpFrom
                 }); 
            }
            if(req.body.token != null && req.body.token !=""){
                let arr = user.token; 
                var found = arr.find(function(element) {
                    return element == req.body.token;
                });
                if(!found){
                    user.token.push(req.body.token);
                    await user.save();
                }
            }
            res.status(200).send({
                user: await User.findOne({phone:req.body.phone}).populate(populateQuery),
                token: generateToken(user.id)
            });
            
        } catch(err){
            next(err);
        }
    },


    
    validateUserCreateBody(isUpdate = false) {
        let validations = [
            
            body('fullname').not().isEmpty().withMessage('fullname is required'),
            body('phone').not().isEmpty().withMessage('phone is required')
            .custom(async (value, { req }) => {
                let userQuery = { phone: value,deleted:false };
                if (isUpdate && req.user.phone === value)
                    userQuery._id = { $ne: req.user._id };

                if (await User.findOne(userQuery))
                    throw new Error(req.__('phone duplicated'));
                else
                    return true;
            }),
           
            body('city').not().isEmpty().withMessage('city is required'),
            body('area').not().isEmpty().withMessage('area is required'),
            body('email').optional()
                .isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value,deleted:false };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),

            body('type').not().isEmpty().withMessage('type is required')
                .isIn(['WORKER','ADMIN','USER']).withMessage('wrong type'),
        ];
        if (!isUpdate) {
            validations.push([
                body('password').optional(),
                body('img').optional().custom(val => isImgUrl(val)).withMessage('card img should be a valid img')
            ]);
        }
        return validations;
    },
    async signUp(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            
            if (req.file) {
               let image = await handleImg(req)
               validatedBody.img = image;
            }
            let adsPackages = await AdsPackage.find({deleted:false,defaultPackage:true});
            console.log(adsPackages)
            let adsPackageId = 1;
            if(adsPackages.length > 0){
                adsPackageId = adsPackages[0].id
            }
            let createdUser;
            if(adsPackages.length > 0){
                console.log(adsPackages)
                validatedBody.adsPackage = adsPackageId
                createdUser = await User.create({
                    ...validatedBody,token:req.body.token
                });
                let adsPackages = await checkExistThenGet(adsPackageId, AdsPackage, { deleted: false });
                adsPackages.usersCount = adsPackages.usersCount + 1;
                await adsPackages.save();

                let user = await checkExistThenGet(createdUser.id, User,{deleted: false });
                user.adsPackage = adsPackages.id;
                user.adsPackageNumber = adsPackages.adsNumber;
                user.hasAdsPackage = true;
                var date = new Date();
                var endDate = new Date(date.setMonth(date.getMonth() + adsPackages.month));
                user.adsPackageStart = new Date();
                user.adsPackageEnd = endDate;
                await user.save();
            }else{
                createdUser = await User.create({
                    ...validatedBody,token:req.body.token
                });
            }
            res.status(201).send({
                user: await User.findById(createdUser.id).populate(populateQuery),
                token: generateToken(createdUser.id)
            });

            let userr = await checkUserExistByPhone(validatedBody.phone);
            userr.verifycode = generateVerifyCode(); 
            
             //send code
            let message =  ' رمز التفعيل الخاص ب Dall هو ' + userr.verifycode
            sendMsg(message,validatedBody.phone)
            await userr.save();
            let reports = {
                "action":"User Sign Up",
            };
            let report = await Report.create({...reports, user: createdUser.id });

        } catch (err) {
            next(err);
        }
    },
    validateWorkerCreatedBody(isUpdate = false) {
        let validations = [
            body('fullname').not().isEmpty().withMessage('fullname is required'),
            body('phone').not().isEmpty().withMessage('phone is required')
            .custom(async (value, { req }) => {
                let userQuery = { phone: value,deleted:false };
                if (isUpdate && req.user.phone === value)
                    userQuery._id = { $ne: req.user._id };

                if (await User.findOne(userQuery))
                    throw new Error(req.__('phone duplicated'));
                else
                    return true;
            }),
            body('nationalID').optional(),
            body('category').not().isEmpty().withMessage('category is required'),
            body('subCategory').not().isEmpty().withMessage('sub Category is required'),
            body('city').not().isEmpty().withMessage('city is required'),
            body('area').not().isEmpty().withMessage('area is required'),
            body('email').optional()
                .isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value,deleted:false };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),

            body('type').not().isEmpty().withMessage('type is required')
                .isIn(['WORKER','ADMIN','USER']).withMessage('wrong type'),
        ];
        if (!isUpdate) {
            validations.push([
                body('password').optional(),
                body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img'),
            ]);
        }
        return validations;
    },
    async signUpWorker(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            
            if (req.file) {
                let image = await handleImg(req,{ attributeName : 'img'})
                validatedBody.img = image;
                
            }
            let createdUser = await User.create({
                ...validatedBody,token:req.body.token
            });

            /*let adsPackages = await AdsPackage.find({deleted:false,defaultPackage:true});
            console.log(adsPackages)
            let adsPackageId = adsPackages[0].id
           
            if(adsPackages.length > 0){
                let adsPackages = await checkExistThenGet(adsPackageId, AdsPackage, { deleted: false });
                adsPackages.usersCount = adsPackages.usersCount + 1;
                await adsPackages.save();
                let user = await checkExistThenGet(createdUser.id, User,{deleted: false });
                user.adsPackage = adsPackages.id;
                user.adsPackageNumber = adsPackages.adsNumber;
                user.hasAdsPackage = true;
                var date = new Date();
                var endDate = new Date(date.setMonth(date.getMonth() + adsPackages.month));
                user.adsPackageStart = new Date();
                user.adsPackageEnd = endDate;
                await user.save();
            }
            */
            let packages = await Package.find({deleted:false,defaultPackage:true});
            let packageId = packages[0].id
            
            if(packages.length > 0){
                let packages = await checkExistThenGet(packageId, Package, { deleted: false });
                packages.usersCount = packages.usersCount + 1;
                await packages.save();

                let user = await checkExistThenGet(createdUser.id, User,{deleted: false });
                user.package = packages.id;
                user.hasPackage = true;
                var date = new Date();
                var endDate = new Date(date.setMonth(date.getMonth() + packages.month));
                user.packageStart = new Date();
                user.packageEnd = endDate;
                await user.save();
            } 
                
            
            res.status(201).send({
                user: await User.findById(createdUser.id).populate(populateQuery),
                token: generateToken(createdUser.id)
            });
            
            let userr = await checkUserExistByPhone(validatedBody.phone);
            userr.verifycode = generateVerifyCode(); 
           
            //send code
            let message =  ' رمز التفعيل الخاص ب Dall هو ' + userr.verifycode
            sendMsg(message,validatedBody.phone)
            await userr.save();
            let reports = {
                "action":"Worker Sign Up",
            };
            let report = await Report.create({...reports, user: createdUser.id });

        } catch (err) {
            next(err);
        }
    },
    async block(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.block = true;
            await activeUser.save();
            let reports = {
                "action":"block User",
            };
            let  report = await Report.create({...reports, user: user });
            res.send('user block');
        } catch (error) {
            next(error);
        }
    },
    async unblock(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.block = false;
            await activeUser.save();
            let reports = {
                "action":"Active User",
            };
            let report = await Report.create({...reports, user: user });
            res.send('user active');
        } catch (error) {
            next(error);
        }
    },
    async active(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.active = true;
            await activeUser.save();
            let reports = {
                "action":"Active User",
            };
            let report = await Report.create({...reports, user: user });
            sendNotifiAndPushNotifi({
                targetUser: userId, 
                fromUser: req.user, 
                text: 'new notification',
                subject: activeUser.id,
                subjectType: 'Dall active your acount'
            });
            let notif = {
                "description":'Dall active your acount',
                "arabicDescription":'تم تفعيل الحساب الخاص بك'
            }
            await Notif.create({...notif,resource:req.user,target:userId,subject:activeUser.id});
            res.send('user active');
            
        } catch (error) {
            next(error);
        }
    },

    async disactive(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.active = false;
            await activeUser.save();
            let reports = {
                "action":"Dis-Active User",
            };
            let report = await Report.create({...reports, user: user });
            res.send('user disactive');
        } catch (error) {
            next(error);
        }
    },

    async findById(req, res, next) {
        try {
            let { id } = req.params;
            await checkExist(id, User, { deleted: false });

            let user = await User.findById(id);
            res.send({user});
        } catch (error) {
            next(error);
        }
    },

    async checkExistEmail(req, res, next) {
        try {
            let email = req.body.email;
            if (!email) {
                return next(new ApiError(400, 'email is required'));
            }
            let exist = await User.findOne({ email: email });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            let reports = {
                "action":"User Check Email Exist Or Not",
            };
            let report = await Report.create({...reports, user: req.user });
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },

    async checkExistPhone(req, res, next) {
        try {
            let phone = req.body.phone;
            if (!phone) {
                return next(new ApiError(400, 'phone is required'));
            }
            let exist = await User.findOne({ phone: phone });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            let reports = {
                "action":"User Check Mobile Exist Or Not",
            };
            let report = await Report.create({...reports, user: req.user });
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },


    validateUpdatedPassword(isUpdate = false) {
        let validation = [
            body('newPassword').optional().not().isEmpty().withMessage('newPassword is required'),
            body('currentPassword').optional().not().isEmpty().withMessage('currentPassword is required')
           
        ];

        return validation;
    },
    async updatePassword(req, res, next) {
        try {
            let user = await checkExistThenGet(req.user._id, User);
            if (req.body.newPassword) {
                if (req.body.currentPassword) {
                    if (bcrypt.compareSync(req.body.currentPassword, user.password)) {
                        user.password = req.body.newPassword;
                    }
                    else {
                        res.status(400).send({
                            error: [
                                {
                                    location: 'body',
                                    param: 'currentPassword',
                                    msg: 'currentPassword is invalid'
                                }
                            ]
                        });
                    }
                }
            }
            await user.save();
            res.status(200).send({
                user: await User.findById(req.user._id)
            });

        } catch (error) {
            next(error);
        }
    },
    //forget password send to email
    validateSendCode() {
        return [
            body('email').not().isEmpty().withMessage('email Required')
        ];
    },
    async sendCodeToEmail(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);
            user.verifycode = generateVerifyCode(); 
            await user.save();
            //send code
            let text = user.verifycode.toString();
            let description = ' verfication code ';
            sendEmail(validatedBody.email, text,description)
            res.status(204).send();
        } catch (error) {
            next(error);
        }
    },
    validateConfirmVerifyCode() {
        return [
            body('verifycode').not().isEmpty().withMessage('verifycode Required'),
            body('email').not().isEmpty().withMessage('email Required'),
        ];
    },
    async resetPasswordConfirmVerifyCode(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);
            if (user.verifycode != validatedBody.verifycode)
                return next(new ApiError.BadRequest('verifyCode not match'));
            /////
            user.active = true;
            await user.save();
            ////
            
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },

    validateResetPassword() {
        return [
            body('email').not().isEmpty().withMessage('email is required'),
            body('newPassword').not().isEmpty().withMessage('newPassword is required')
        ];
    },

    async resetPassword(req, res, next) {
        try {

            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);

            user.password = validatedBody.newPassword;
            await user.save();
           
            res.status(204).send();

        } catch (err) {
            next(err);
        }
    },
    validateForgetPassword() {
        return [
            body('phone').not().isEmpty().withMessage('Phone Required')
        ];
    },
    async forgetPasswordSms(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let realPhone = validatedBody.phone;
            let user = await checkUserExistByPhone(validatedBody.phone);

            user.verifycode = generateVerifyCode();
            await user.save();
             //send code
            let message =  ' رمز التفعيل الخاص ب Dall هو ' + user.verifycode
            sendMsg(message,realPhone)
            let reports = {
                "action":"send verify code to user",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();
        } catch (error) {
            next(error);
        }
    },
    validateConfirmVerifyCodePhone() {
        return [
            body('phone').not().isEmpty().withMessage('phone Required'),
            body('verifycode').not().isEmpty().withMessage('verifycode Required'),
        ];
    },
    async resetPasswordConfirmVerifyCodePhone(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByPhone(validatedBody.phone);
            if (user.verifycode != validatedBody.verifycode)
                return next(new ApiError.BadRequest('verifyCode not match'));
            user.active = true;
            await user.save();
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },
    validateResetPasswordPhone() {
        return [
            body('phone').not().isEmpty().withMessage('phone is required'),
            body('newPassword').not().isEmpty().withMessage('newPassword is required')
        ];
    },

    async resetPasswordPhone(req, res, next) {
        try {

            let validatedBody = checkValidations(req);
            let user = await checkUserExistByPhone(validatedBody.phone);
            user.password = validatedBody.newPassword;
            await user.save();
            let reports = {
                "action":"User reset Password",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();

        } catch (err) {
            next(err);
        }
    },

    async updateToken(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.newToken;
            });
            if(!found){
                users.token.push(req.body.newToken);
                await users.save();
            }
            let oldtoken = req.body.oldToken;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == oldtoken){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    async logout(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            let token = req.body.token;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == token){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {type, active,area,city,category,subCategory,name,sortByRate,sortByWeekRate} = req.query;
            
            let query = {deleted: false };
           
            if (type) query.type = type;
            if (area) query.area = area;
            if (city) query.city = city;
            if (category) query.category = category;
            if (subCategory) query.subCategory = subCategory;
            if (active=="true") query.active = true;
            if (active=="false") query.active = false;
            if(name){
                query.fullname = { $regex: '.*' + name + '.*' }
            }  
            let sort = {createdAt: -1}
            if (sortByRate) sort = {rate:1};
            if (sortByWeekRate) sort = {weekRate:1};
            let users = await User.find(query).populate(populateQuery)
                .sort(sort)
                .limit(limit)
                .skip((page - 1) * limit);

            const usersCount = await User.count(query);
            const pageCount = Math.ceil(usersCount / limit);
            res.send(new ApiResponse(users, page, pageCount, limit, usersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findWorkersOfWeek(req, res, next) {
        try {
            let query = {deleted: false };
           
            var d = new Date();
            let from = new Date(d.setDate(d.getDate() - 7));

            let to = new Date();
            console.log(Date.parse(from))
            console.log(Date.parse(to))

            query = {
                $and: [
                    {date: { $gte : from, $lte : to }},
                    {deleted: false } ,
                ]
            };
            let rates = await Rate.find(query).select("worker rate")
            let data=[];
            let usersIds = [];
            rates.forEach(id => {
                usersIds.push(id.worker)
            });  
            usersIds = [ ...new Set(usersIds) ];
            
            let newRates = [];
            let ratesCount;
            for(var i=0; i< usersIds.length;i++){
                let query2 = {
                    $and: [
                        {date: { $gte : from, $lte : to }},
                        {deleted: false } ,
                        {worker:usersIds[i]}
                    ]
                };
                ratesCount = await Rate.find(query2).populate(populateQuery2)
                .select('worker rate')
                .then(async data => {
                    var newdata = [];
                    data.map(function (element) {
                        newdata.push({
                            worker: element.worker,
                            rates: element.rate,
                            
                        });
                    })
                    newRates.push(newdata)  
                });
            } 
            var data = [];
            for(var i=0; i< newRates.length;i++){
                var total=0;
                var newdata = [];
                for(var j in newRates[i]) { 
                    total += parseInt(newRates[i][j].rates); 
                    
                }
                newdata.push({
                    worker: newRates[i][0].worker,
                    rates: total,
                    
                });
                data.push(newdata)

            }
           
            let finalData = [];
            data.forEach(id => {
                finalData.push(id[0])
            });
            
            finalData.sort((a, b) => {
                return a.rates - b.rates;
            });
            finalData.reverse();
           
            res.send(finalData);
        } catch (err) {
            next(err);
        }
    },
    
    async delete(req, res, next) {
        try {
            let {userId } = req.params;
            if (req.user.type != 'ADMIN')
            return next(new ApiError(403, ('admin.auth'))); 
            let user = await checkExistThenGet(userId, User,
                {deleted: false });
            user.deleted = true
            await user.save();
            let reports = {
                "action":"Delete user",
            };
            let report = await Report.create({...reports, user: req.user._id });
            res.status(204).send();
        }
        catch (err) {
            next(err);
        }
    },
    async tracking(req, res, next) {
        try {
            let {userId } = req.params;
            let user = await checkExistThenGet(userId, User,{deleted: false });
            user.location = req.body.location
            await user.save();
           
            res.status(204).send();
        }
        catch (err) {
            next(err);
        }
    },
   
    validateUpdatedUser(isUpdate = true) {
        let validation = [
            body('fullname').not().isEmpty().withMessage('fullname is required'),
            body('phone').not().isEmpty().withMessage('phone is required')
            .custom(async (value, { req }) => {
                let {userId} = req.params;
                let user = await checkExistThenGet(userId, User);
                let userQuery = { phone: value ,deleted:false};
                if (isUpdate && user.phone === value)
                    userQuery._id = { $ne: userId };

                if (await User.findOne(userQuery))
                    throw new Error(req.__('phone duplicated'));
                else
                    return true;
            }),
            body('nationalID').optional(),
            body('category').optional(),
            body('subCategory').optional(),
            body('bio').optional(),
            body('city').not().isEmpty().withMessage('city is required'),
            body('area').not().isEmpty().withMessage('area is required'),
            body('email').optional()
                .custom(async (value, { req }) => {
                    let {userId} = req.params;
                    let user = await checkExistThenGet(userId, User);
                    let userQuery = { email: value,deleted:false };
                    if (isUpdate && user.email === value)
                        userQuery._id = { $ne: userId };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
            }),

            body('type').not().isEmpty().withMessage('type is required')
                .isIn(['WORKER','ADMIN','USER']).withMessage('wrong type'),
        ];
        if (isUpdate)
            validation.push([
                body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img'),
            ]);

        return validation;
    },
    async updateUser(req, res, next) {
        try {
           
            const validatedBody = checkValidations(req);
            let {userId} = req.params;
            let user = await checkExistThenGet(userId, User);
            
            if (req.file) {
                let image = await handleImg(req)
                validatedBody.img = image;
                user.img = validatedBody.img
             }
            if(validatedBody.phone){
                user.phone = validatedBody.phone;
            }
            if(validatedBody.fullname){
                user.fullname = validatedBody.fullname;
            }
            if(validatedBody.email){
                user.email = validatedBody.email;
            }
            if(validatedBody.city){
                user.city = validatedBody.city;
            }
            if(validatedBody.area){
                user.area = validatedBody.area;
            }
            if(validatedBody.category){
                user.category = validatedBody.category;
            }
            if(validatedBody.subCategory){
                user.subCategory = validatedBody.subCategory;
            }
            if(validatedBody.nationalID){
                user.nationalID = validatedBody.nationalID;
            }
       
            
            if(validatedBody.type){
                user.type = validatedBody.type;
            }
            if(validatedBody.bio){
                user.bio = validatedBody.bio;
            }
        
           
            
            await user.save();
           
            res.status(200).send({
                user: await User.findById(userId).populate(populateQuery),
            });


        } catch (error) {
            next(error);
        }
    },
    async buyPackage(req, res, next) {
        try {
            let {packageId } = req.params;
            let packages = await checkExistThenGet(packageId, Package, { deleted: false });
            packages.usersCount = packages.usersCount + 1;
            await packages.save();

            let user = await checkExistThenGet(req.user._id, User,{deleted: false });
            user.package = packageId;
            user.hasPackage = true;
            var date = new Date();
            var endDate = new Date(date.setMonth(date.getMonth() + packages.month));
            user.packageStart = new Date();
            user.packageEnd = endDate;
            await user.save();
                
            res.send(user);
        }
        catch (err) {
            next(err);
        }
    },
    async buyAdsPackage(req, res, next) {
        try {
            let {packageId } = req.params;
            let packages = await checkExistThenGet(packageId, AdsPackage, { deleted: false });
            packages.usersCount = packages.usersCount + 1;
            await packages.save();

            let user = await checkExistThenGet(req.user._id, User,{deleted: false });
            user.adsPackage = packageId;
            user.adsPackageNumber = packages.adsNumber
            user.hasAdsPackage = true;
            var date = new Date();
            var endDate = new Date(date.setMonth(date.getMonth() + packages.month));
            user.adsPackageStart = new Date();
            user.adsPackageEnd = endDate;
            await user.save();
                
            res.send(user);
        }
        catch (err) {
            next(err);
        }
    },
    async addToGallery(req, res, next) {
        try {
            let {userId } = req.params;
            let user = await checkExistThenGet(userId, User,{deleted: false });
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img'});
                user.gallery.push(image);
                await user.save();
                
            }
            res.send(user);
        }
        catch (err) {
            next(err);
        }
    },
    async removeFromGallery(req, res, next) {
        try {
            let {userId } = req.params;
            let user = await checkExistThenGet(userId, User,{deleted: false });
            let arr = user.gallery;
            let index = parseInt(req.body.index);
            console.log(index)
            for(var i = 0;i<= arr.length;i=i+1){
                if(arr[i] === arr[index]){
                    console.log(arr[i]);
                    console.log(arr[index]);
                    arr.splice(index, 1);
                }
            }

            user.gallery = arr;
            await user.save();
            res.send(user);
        }
        catch (err) {
            next(err);
        }
    },



};
