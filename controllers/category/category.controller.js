import ApiResponse from "../../helpers/ApiResponse";
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Category from "../../models/category/category.model";
import SubCategory from "../../models/category/sub-category.model";
import User from "../../models/user/user.model";

const populateQuery = [ 
    { path: 'child', model: 'category' },
    { path: 'parent', model: 'category' },
];

export default {


    async findCategoryPagenation(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { main} = req.query;
            
            let query = { deleted: false, parent: { $exists: false }};
            if (main)
                query.main = main;
            let categories = await Category.find(query).populate(populateQuery)
                .sort({ priority: -1 })
                .limit(limit)
                .skip((page - 1) * limit);

            const categoriesCount = await Category.count(query);
            const pageCount = Math.ceil(categoriesCount / limit);

            res.send(new ApiResponse(categories, page, pageCount, limit, categoriesCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllSubCategoryPagenation(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { main} = req.query;
            
            let query = { deleted: false};
            if (main)
                query.main = main;
            let categories = await SubCategory.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);

            const categoriesCount = await SubCategory.count(query);
            const pageCount = Math.ceil(categoriesCount / limit);

            res.send(new ApiResponse(categories, page, pageCount, limit, categoriesCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllSubCategoryWithoutPagenation(req, res, next) {
        try {
           
            let query = { deleted: false};
           
            let categories = await SubCategory.find(query).populate(populateQuery)
               
            res.send(categories);
        } catch (err) {
            next(err);
        }
    },
    async findSubCategoryPagenation(req, res, next) {
        try {
            let { categoryId } = req.params,
                page = +req.query.page || 1,
                limit = +req.query.limit || 20;

            await checkExist(categoryId, Category);

            let query = { parent: categoryId, deleted: false };
            let categories = await SubCategory.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            console.log(categories)

            const categoriesCount = await SubCategory.count(query);
            const pageCount = Math.ceil(categoriesCount / limit);

            res.send(new ApiResponse(categories, page, pageCount, limit, categoriesCount, req));

        } catch (error) {
            next(error);
        }
    },
    async findCategory(req, res, next) {
        try {            
            let query = { deleted: false,main:true};
            let categories = await Category.find(query).populate(populateQuery)
                .sort({ createdAt: -1 });
            res.send(categories)
        } catch (err) {
            next(err);
        }
    },
    async findSubCategory(req, res, next) {
        try {
            let { categoryId } = req.params;
            await checkExist(categoryId, Category);
            let query = { parent: categoryId, deleted: false,main:false };
            let categories = await SubCategory.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
            res.send(categories);

        } catch (error) {
            next(error);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('name').not().isEmpty().withMessage('name is required')
                .custom(async (val, { req }) => {
                    let query = { name: val, deleted: false };

                    if (isUpdate)
                        query._id = { $ne: req.params.categoryId };

                    let category = await Category.findOne(query).lean();
                    console.log(category)
                    if (category)
                        throw new Error('category duplicated name');

                    return true;
                }),
            body('arabicName').not().isEmpty().withMessage('arabic name is required')
                .custom(async (val, { req }) => {
                    let query = { name: val, deleted: false };

                    if (isUpdate)
                        query._id = { $ne: req.params.categoryId };

                    let category = await Category.findOne(query).lean();
                    if (category)
                        throw new Error('category duplicated name');

                    return true;
                }),
            body('parent').optional().withMessage('parent is required'),
            body('details').optional(),
            body('main').optional(),
            body('type').optional()
            .isIn(['QUICK-SERVICE','HOME-SERVICE','HAND-MADE']).withMessage('wrong type'),
        ];
        if (isUpdate)
            validations.push([
                body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
            ]);

        return validations;
    },

    async create(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let model;
            if(validatedBody.main){
                validatedBody.main = true
            }
            if (validatedBody.parent) {
                let parentCategory = await checkExistThenGet(validatedBody.parent, Category);
                parentCategory.hasChild = true;
                await parentCategory.save();
                model = SubCategory;
            }
            else {
                model = Category;
            }

            let image = await handleImg(req);

            let createdCategory = await model.create({ ...validatedBody, img: image });
            if(model == SubCategory){
                let parentCategory = await checkExistThenGet(validatedBody.parent, Category);
                parentCategory.child.push(createdCategory._id);
                await parentCategory.save();
            }
            res.status(201).send(createdCategory);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { categoryId } = req.params;
            await checkExist(categoryId, Category, { deleted: false });
            let category = await Category.findById(categoryId).populate(populateQuery)
            res.send(category);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let { categoryId } = req.params, model;
            await checkExist(categoryId, Category, { deleted: false });

            const validatedBody = checkValidations(req);


            if (validatedBody.parent) {
                let parentCategory = await checkExistThenGet(validatedBody.parent, Category);
                parentCategory.hasChild = true;
                await parentCategory.save();
                model = SubCategory;
            }
            else {
                model = Category;
            }

            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }

            let updatedCategory = await model.findByIdAndUpdate(categoryId, {
                ...validatedBody,
            }, { new: true });
            if(model == SubCategory){
                let parentCategory = await checkExistThenGet(validatedBody.parent, Category);
                parentCategory.child.push(createdCategory._id);
                await parentCategory.save();
            }

            res.status(200).send(updatedCategory);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let { categoryId } = req.params;

            let category = await checkExistThenGet(categoryId, Category, { deleted: false });

             /* delete category from her parent child array */
            if(category.parent){
                let parentCategory = await checkExistThenGet(category.parent, Category, { deleted: false });
                let arr = parentCategory.child;
                console.log(arr);
                for(let i = 0;i<= arr.length;i=i+1){
                    if(arr[i] == category.id){
                        arr.splice(i, 1);
                    }
                }
                parentCategory.child = arr;
                await parentCategory.save();
            }
            /* delete all category children */
            if(category.hasChild == true){
                let childs = await SubCategory.find({parent : categoryId });
                console.log(childs)
                for (let child of childs ) {
                    console.log(child)
                    child.deleted = true;
                    await child.save();
                }
            }
            /* delete users under category */
            let users = await User.find({
                $or: [
                    {category : categoryId},
                    {subCategory : categoryId}, 
                ]  
            });
            for (let user of users ) {
                user.deleted = true;
                await user.save();
            }
             /*finish delete users under category */
            category.deleted = true;

            await category.save();

            res.status(204).send();

        }
        catch (err) {
            next(err);
        }
    },


};