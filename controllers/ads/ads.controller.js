import ApiResponse from "../../helpers/ApiResponse";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet} from "../../helpers/CheckMethods";
import { handleImgs, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Ads from "../../models/ads/ads.model";
import User from "../../models/user/user.model";
import AdsPackage from "../../models/adsPackage/adsPackage.model";
import { toImgUrl } from "../../utils";

const populateQuery = [
    { path: 'user', model: 'user' },
    { path: 'area', model: 'area' },
    { path: 'target', model: 'category' },
];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20 ,
            { area,user,target,all,type,city,adsPackage,packageType,active} = req.query;
            let curr = Date.parse(new Date());
            console.log(curr)

            let query = {
                $and: [
                    {end: { $gte : curr}},
                    {deleted: false } ,
                    {active: true } ,
                ]
            };
            if(all){
                query ={deleted: false}
            }
            if (active == "true")
                query.active = true;

            if (active == "false")
                query.active = false;
            if (area)
                query.area = area;
            if (city)
                query.city = city;
            if (user)
                query.user = user;
            if (target)
                query.target = target;
            if (type)
                query.type =type;
            if (adsPackage)
                query.package = adsPackage;
            if (packageType)
                query.packageType = packageType;

            let ads = await Ads.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit) 
                .skip((page - 1) * limit);
            for (let ad of ads) {
                let theAd = await checkExistThenGet(ad._id, Ads)
                    let curr = Date.parse(new Date())
                    if(curr > theAd.end){
                        theAd.active = false;
                    }
                await theAd.save();
            }
            ads = await Ads.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            const AdssCount = await Ads.count(query);
            const pageCount = Math.ceil(AdssCount / limit);

            res.send(new ApiResponse(ads, page, pageCount, limit, AdssCount, req));
        } catch (err) {
            next(err);
        }
    },
    

    validateBody(isUpdate = false) {
        let validations = [
            body('description').not().isEmpty().withMessage('description is required'),
            body('title').not().isEmpty().withMessage('title is required'),
            body('type').not().isEmpty().withMessage('type is required')
            .isIn(['FREE','PREMIUM','DALL']).withMessage('wrong type'),
            body('area').not().isEmpty().withMessage('area is required'),
            body('city').optional(),
            body('phone').optional(),
            body('email').optional(),
            body('link').optional(),
            body('lat').optional(),
            body('lang').optional(),
            body('target').optional(),


        ];
        if (isUpdate)
            validations.push([
                body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
            ]);

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            const validatedBody = checkValidations(req);
            validatedBody.user = req.user._id;
            validatedBody.active = true;
            console.log(validatedBody);
            if(validatedBody.lat && validatedBody.lang){
                validatedBody.location = [validatedBody.lat,validatedBody.lang]
            }
            if (req.files) {
                let images = await handleImgs(req, { attributeName: 'img'});
                validatedBody.img = images;
            }
            let createdAds;
            user = await checkExistThenGet(req.user._id, User,{deleted: false });
            if(user.type != 'ADMIN'){
                if(user.adsPackage && user.adsPackageNumber > 0){
            
                    let curr = Date.parse(new Date());
                    console.log(curr)
                    let to = Date.parse(user.adsPackageEnd)
                    console.log(to)
                    if(to > curr){
                        let packages = await checkExistThenGet(user.adsPackage, AdsPackage,{deleted: false });
                        var date = new Date();
                        var endDate = new Date(date.setMonth(date.getMonth() + packages.month));
                        
                        validatedBody.start = Date.parse(new Date());
                        validatedBody.end = Date.parse(endDate);
                        validatedBody.package = user.adsPackage;
                        validatedBody.packageType = packages.type;
                        console.log(validatedBody.start)
                        console.log(validatedBody.end)
                        createdAds = await Ads.create({ ...validatedBody});
                        user.adsPackageNumber = user.adsPackageNumber - 1;
                        await user.save();
                        let reports = {
                            "action":"Create Ads",
                        };
                        let report = await Report.create({...reports, user: user });
                    } else{
                        return next(new ApiError(400, ('your Ads Package has been expire')));
                    }
                    
                }else{
                    return next(new ApiError(400, ('please buy an Ads Package')));
                }
               
            } 
            if(user.type == 'ADMIN'){
                validatedBody.start = Date.parse(new Date());
                if(!req.body.endDate){
                    return next(new ApiError(422, ('enter ads end date')));
                }
                validatedBody.end = Date.parse(req.body.endDate);
                if(req.body.adsPackage){
                    let packages = await checkExistThenGet(req.body.adsPackage, AdsPackage,{deleted: false });
                    validatedBody.packageType = packages.type;
                    validatedBody.package = req.body.adsPackage;
                }
                console.log(validatedBody.start)
                console.log(validatedBody.end)
                createdAds = await Ads.create({ ...validatedBody});
                let reports = {
                    "action":"Create Ads",
                };
                let report = await Report.create({...reports, user: user });
            } 
            user = await checkExistThenGet(req.user._id, User,{deleted: false });
            res.status(201).send(user);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { AdsId } = req.params;
            await checkExist(AdsId, Ads, { deleted: false });
            let ads = await Ads.findById(AdsId);
            res.send(ads);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
           
            let { AdsId } = req.params;
            await checkExist(AdsId, Ads, { deleted: false });
            const validatedBody = checkValidations(req);
            if(validatedBody.lat && validatedBody.lang){
                validatedBody.location = [validatedBody.lat,validatedBody.lang]
            }
            if (req.files) {
                if (req.files['img']) {
                    let imagesList = [];
                    for (let imges of req.files['img']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.img = imagesList;
                }
            }
            let updatedAds = await Ads.findByIdAndUpdate(AdsId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update Ads",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedAds);
        }
        catch (err) {
            next(err);
        }
    },
    async createAds(req, res, next) {
        try {
            let user = req.user;
            const validatedBody = checkValidations(req);
            validatedBody.user = req.user._id;
            console.log(validatedBody);
            if(validatedBody.lat && validatedBody.lang){
                validatedBody.location = [validatedBody.lat,validatedBody.lang]
            }
            if (req.files) {
                let images = await handleImgs(req, { attributeName: 'img'});
                validatedBody.img = images;
            }
            let createdAds = await Ads.create({ ...validatedBody});
            res.send(createdAds);
        }
        catch (err) {
            next(err);
        }
    },
    async active(req, res, next) {

        try {
            let user = req.user;
            let { AdsId } = req.params;
            let ads = await checkExistThenGet(AdsId, Ads, { deleted: false });
            ads.active = true;
            user = await checkExistThenGet(req.user._id, User,{deleted: false });
            if(user.type != 'ADMIN'){
                if(user.adsPackage && user.adsPackageNumber > 0){
                    let curr = Date.parse(new Date());
                    console.log(curr)
                    let to = Date.parse(user.adsPackageEnd)
                    console.log(to)
                    if(to > curr){
                        let packages = await checkExistThenGet(user.adsPackage, AdsPackage,{deleted: false });
                        var date = new Date();
                        var endDate = new Date(date.setMonth(date.getMonth() + packages.month));
                        ads.start = Date.parse(new Date());
                        ads.end = Date.parse(endDate);
                        ads.package = user.adsPackage;
                        ads.packageType = packages.type;
                        await ads.save();
                        user.adsPackageNumber = user.adsPackageNumber - 1;
                        await user.save();
                    } else{
                        return next(new ApiError(400, ('your Ads Package has been expire')));
                    }
                    
                }else{
                    return next(new ApiError(400, ('please buy an Ads Package')));
                }
            } 
            user = await checkExistThenGet(req.user._id, User,{deleted: false });
            res.status(201).send(user);
        } catch (err) {
            next(err);
        }
    },
    async delete(req, res, next) {
        try {
            let user = req.user;
            
            let { AdsId } = req.params;
            let ads = await checkExistThenGet(AdsId, Ads, { deleted: false });
            
            ads.deleted = true;
            await ads.save();
            let reports = {
                "action":"Delete Ads",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};