import { checkExist, checkExistThenGet, isLng, isLat, isArray, isNumeric } from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import Offer from "../../models/offer/offer.model";
import Order from "../../models/order/order.model";

import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import { body } from "express-validator/check";
import { ValidationError } from "mongoose";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import Notif from "../../models/notif/notif.model"
import { off } from "npm";

const populateQuery = [
    { path: 'order', model: 'order' },
    { path: 'worker', model: 'user' },
    { path: 'client', model: 'user' },
    {
        path: 'order', model: 'order',
        populate: { path: 'category', model: 'category' },
    },
    {
        path: 'order', model: 'order',
        populate: { path: 'subCategory', model: 'category' },
    },
    {
        path: 'worker', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'worker', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
     
];

const populateQuery2 = [
    { path: 'city', model: 'city' },
    { path: 'area', model: 'area' },
    { path: 'category', model: 'category' },
    { path: 'subCategory', model: 'category' },
];
function validatedestination(location) {
    if (!isLng(location[0]))
        throw new ValidationError.UnprocessableEntity({ keyword: 'location', message: 'location[0] is invalid lng' });
    if (!isLat(location[1]))
        throw new ValidationError.UnprocessableEntity({ keyword: 'location', message: 'location[1] is invalid lat' });
}

var OfferController = {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20
                ,{ accept,order,worker,rejected } = req.query
                , query = {deleted: false };

            
            if (order){
                query.order = order;
            } 
            if (worker){
                query.worker = worker;
            } 
            if (rejected)
                query.rejected = rejected;
            if(rejected == 'true')
                query.rejected = true
            if(rejected == 'false')
                query.rejected = false
            if(accept == 'true')
                query.accept = true

            if(accept == 'false')
                query.accept = false
            let offers = await Offer.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const offersCount = await Offer.count(query);
            const pageCount = Math.ceil(offersCount / limit);

            res.send(new ApiResponse(offers, page, pageCount, limit, offersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async countOffer(req,res, next) {
        try {
            let { accept,order,worker,rejected,client } = req.query
            , query = {deleted: false };
            
            if(accept == 'true')
                query.accept = true

            if(accept == 'false')
                query.accept = false

            if (order){
                query.order = order;
            } 
            if (worker){
                query.worker = worker;
            } 
            
            if(rejected == 'true')
                query.rejected = true
            if(rejected == 'false')
                query.rejected = false
            if (client)
                query.client = client;
            const offerCount = await Offer.count(query);
            
            res.status(200).send({
                offer:offerCount   
            });
        } catch (err) {
            next(err);
        }
        
    },
    
    validateCreatedOffers() {
        let validations = [
            body('cost').not().isEmpty().withMessage('cost is required'),

        ];
        return validations;
    },
            
    async create(req, res, next) {
        try {
            let {orderId} = req.params;
            await checkExist(req.user._id, User);
            const validatedBody = checkValidations(req);
            if (req.user.type != 'WORKER')
            return next(new ApiError(403, ('worker auth')));
            
            let order = await checkExistThenGet(orderId, Order);
            validatedBody.order = orderId;
            validatedBody.worker = req.user;
            
            let createdOffer = await Offer.create({ ...validatedBody});
            let offer = await Offer.populate(createdOffer, populateQuery);
            
            let reports = {
                "action":"worker Create New Offer",
            };
            let report = await Report.create({...reports, user: req.user });
            sendNotifiAndPushNotifi({
                targetUser: order.client, 
                fromUser: req.user._id, 
                text: 'Dall',
                subject: offer.id,
                subjectType: 'لديك عرض جديد على طلبك'
            });
            let notif = {
                "description":'  a new offer on your order',
                "arabicDescription":' لديك عرض جديد على طلبك '

            }
            Notif.create({...notif,resource:req.user._id,target:order.client,offer:offer.id});
        
            
            res.status(201).send(offer);
        } catch (err) {
            next(err);
            
        }
    },
    async addOffer(io, nsp, data) {
        //orderId workerId  deliveryCost 
        let order = await checkExistThenGet(data.orderId, Order);
        var toRoom = 'room-' + order.client; 
        console.log(order.client)
        let validatedBody = {};
        if(data.cost != null){
            validatedBody.cost = data.cost;
        }
        if(data.description != null){
            validatedBody.description = data.description;
        }
        
        await checkExist(data.workerId, User);
        let user = await checkExistThenGet(data.workerId, User);
        validatedBody.order = data.orderId;
        validatedBody.worker = data.workerId;
        validatedBody.client = order.client;

        var query = {
            order: data.orderId,
            deleted: false,
        }
        var query3 = {
            order: data.orderId,
            deleted: false,
            worker:data.workerId
        }
        if(await Offer.findOne(query3)){
            Offer.updateMany(query, { lastOffer: true,cost:validatedBody.cost })
            .then(async (result1) => {
                let query2 = {deleted: false,order: data.orderId,lastOffer: true };
                Offer.find(query2).populate(populateQuery)
                .then(async (data) => {
                    nsp.to(toRoom).emit('newOffer', { offer: data});
                    sendNotifiAndPushNotifi({
                        targetUser: order.client, 
                        fromUser: offer.worker, 
                        text: 'Dall',
                        subject: offer.id,
                        subjectType: 'لديك عرض جديد على طلبك'
                    });
                    let notif = {
                        "description": 'new offer on your order  ',
                        "arabicDescription":' لديك عرض جديد على طلبك '
                    }
                    Notif.create({...notif,resource:offer.worker,target:order.client,offer:offer.id});
                });
                
            }).catch((err) => {
                console.log(err);
            });

        }else{
            
            Offer.updateMany(query, { lastOffer: false })
            .then(async (result1) => {
               
                let user = await User.findById(data.workerId);

                var offer = new Offer(validatedBody);
                console.log(offer);
                let query2 = {deleted: false,order: data.orderId,lastOffer: true };
                offer.save()
                    .then(result2 => {
                        Offer.find(query2).populate(populateQuery)
                        .then(async (data) => {
                            nsp.to(toRoom).emit('newOffer', { offer: data});
                            sendNotifiAndPushNotifi({
                                targetUser: order.client, 
                                fromUser: offer.worker, 
                                text: 'Dall',
                                subject: offer.id,
                                subjectType: 'لديك عرض جديد على طلبك'
                            });
                            let notif = {
                                "description": 'new offer on your order  ',
                                "arabicDescription":' لديك عرض جديد على طلبك '
                            }
                            Notif.create({...notif,resource:offer.worker,target:order.client,offer:offer.id});
                        
                        });
                    })
                    .catch(err => {
                        console.log('can not save offer .')
                        console.log(err);
                    });
            
            
            }).catch((err) => {
                console.log(err);
            });
        }
    },
    
    async findById(req, res, next) {
        try {
            let {offerId } = req.params;
            let offer = await  Offer.find({_id:offerId});
            res.send(
                offer
            );
        } catch (err) {
            next(err);
        }
    },
    async refuse(req, res, next) {
        try {
            let { offerId } = req.params;
            let offer = await checkExistThenGet(offerId, Offer);
            offer.accept = false;
            offer.rejected = true;
            await offer.save();
            
            sendNotifiAndPushNotifi({
                targetUser: offer.worker, 
                fromUser: req.user, 
                text: 'Dall',
                subject: offer.id,
                subjectType: 'تم رفض العرض'
            });
            let notif = {
                "description":'user refuse your offer',
                "arabicDescription":'تم رفض العرض الخاص بك'
            }
            await Notif.create({...notif,resource:req.user,target:offer.worker,offer:offer.id});
            res.send(offer);
            
        } catch (error) {
            next(error);
        }
    },
    async accept(req, res, next) {
        try {
            let { offerId } = req.params;
            let offer = await checkExistThenGet(offerId, Offer);
            let order = await checkExistThenGet(offer.order, Order);
            let worker = await User.find({_id:offer.worker}).populate(populateQuery2);
            
            if (order.offer)
            return next(new ApiError(403, ('this order have offer')));
            let offers = await Offer.find({ order: order._id });
            for (let offer of offers ) {
                //offer.deleted = true;
                offer.rejected = true;
                await offer.save();
            }
            order.status = "CONFIRMED";
            order.worker = offer.worker;
            order.accept = true;
            
            order.offer = offerId;
            await order.save();
            
            offer.accept = true;
            offer.rejected = false;
            await offer.save();

           
            
            let workers = await Offer.find({deleted:false,order:order._id});
            console.log(workers);
            workers.forEach(worker => {
                console.log(worker.worker)
                if(worker != order.worker){
                    sendNotifiAndPushNotifi({////////
                        targetUser: worker.worker, 
                        fromUser: "Dall", 
                        text: 'Dall',
                        subject: order.id,
                        subjectType: 'احد مندوبينا حصل على الطلب '
                    });
                    let notif = {
                        "description":'this offer has been taken',
                        "arabicDescription":"احد مندوبينا حصل على الطلب الذى قمت بتقديم عرض عليه"
                    }
                    Notif.create({...notif,resource:req.user._id,target:worker.worker,order:order.id});
                }
                
            });

            sendNotifiAndPushNotifi({
                targetUser: offer.worker, 
                fromUser: req.user, 
                text: 'Dall',
                subject: offer.id,
                subjectType: ' تمت الموافقه على عرضك'
            });
            let notif = {
                "description":' your offer has been accepted',
                "arabicDescription":' تمت الموافقه على عرضك'
                
            }
            await Notif.create({...notif,resource:req.user,target:offer.worker,offer:offer.id});
            let theOffer = await Offer.find({_id:offerId}).populate(populateQuery);
            console.log(theOffer)
            res.send(theOffer);
            
        } catch (error) {
            next(error);
        }
    },
    
    async delete(req, res, next) {
        try {
            let { offerId } = req.params;
            let offer = await checkExistThenGet(offerId, Offer);

            offer.deleted = true;
            await offer.save();
            let reports = {
                "action":"Delete offer",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
};
module.exports = OfferController;
