import User from "../../models/user/user.model";
import { checkExistThenGet, checkExist} from "../../helpers/CheckMethods";
import Notif from "../../models/notif/notif.model";
import ApiResponse from "../../helpers/ApiResponse";
const populateQuery = [
   
    {
        path: 'target', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'target', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
    { path: 'target', model: 'user' },
    {
        path: 'offer', model: 'offer',
        populate: { path: 'order', model: 'order' },
    },
    {
        path: 'offer', model: 'offer',
        populate: { 
            path: 'order', model: 'order' ,
            populate: { path: 'category', model: 'category' }
        },
    },
    {
        path: 'offer', model: 'offer',
        populate: { 
            path: 'order', model: 'order' ,
            populate: { path: 'subCategory', model: 'category' }
        },
    },

    {
        path: 'order', model: 'order',
        populate: { path: 'category', model: 'category' },
    },
    {
        path: 'order', model: 'order',
        populate: { path: 'subCategory', model: 'category' },
    },
    {
        path: 'resource', model: 'user',
        populate: { path: 'city', model: 'city' },
    },
    {
        path: 'resource', model: 'user',
        populate: { path: 'area', model: 'area' },
    },
    {
        path: 'resource', model: 'user',
        populate: { path: 'category', model: 'category' },
    },
    {
        path: 'resource', model: 'user',
        populate: { path: 'subCategory', model: 'category' },
    },
    
    {
        path: 'order.worker', model: 'user',
        populate: { path: 'category', model: 'category' },
    },

    {
        path: 'order.worker', model: 'user',
        populate: { path: 'subCategory', model: 'category' },
    },


];
export default {
    async find(req, res, next) {
        try {
            let user = req.user._id;
            await checkExist(req.user._id, User);
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false,target:user };
            let notifs = await Notif.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const notifsCount = await Notif.count(query);
            const pageCount = Math.ceil(notifsCount / limit);

            res.send(new ApiResponse(notifs, page, pageCount, limit, notifsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async unreadCount(req, res, next) {
        try {
            let user = req.user._id;
            await checkExist(req.user._id, User);
            let query = { deleted: false,target:user,read:false };
            const unreadCount = await Notif.count(query);
            res.status(200).send({
                unread:unreadCount,
            });
        } catch (err) {
            next(err);
        }
    },
    async read(req, res, next) {
        try {
            let { notifId} = req.params;
            let notif = await checkExistThenGet(notifId, Notif);
            notif.read = true;
            await notif.save();
            res.send('notif read');
        } catch (error) {
            next(error);
        }
    },

    async unread(req, res, next) {
        try {
            let { notifId} = req.params;
            let notif = await checkExistThenGet(notifId, Notif);
            notif.read = false;
            await notif.save();
            res.send('notif unread');
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        try {
            let { notifId} = req.params;
            let notif = await checkExistThenGet(notifId, Notif);
            notif.deleted = true;
            await notif.save();
            res.send('notif deleted');
        } catch (error) {
            next(error);
        }
    },
}