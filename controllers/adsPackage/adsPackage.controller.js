import ApiResponse from "../../helpers/ApiResponse";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet} from "../../helpers/CheckMethods";
import { checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import AdsPackage from "../../models/adsPackage/adsPackage.model";
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20 ,
             query = {deleted: false };
           
            let adsPackages = await AdsPackage.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const adsPackagesCount = await AdsPackage.count(query);
            const pageCount = Math.ceil(adsPackagesCount / limit);

            res.send(new ApiResponse(adsPackages, page, pageCount, limit, adsPackagesCount, req));
        } catch (err) {
            next(err);
        }
    },
   
    validateBody(isUpdate = false) {
        let validations = [
            body('month').not().isEmpty().withMessage('month is required'),
            body('cost').not().isEmpty().withMessage('cost is required'),
            body('description').not().isEmpty().withMessage('description is required'),
            body('adsNumber').not().isEmpty().withMessage('ads number is required'),
            body('arabicDescription').not().isEmpty().withMessage('arabic description is required'),
        ];
        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin only')));
    
            const validatedBody = checkValidations(req);
           
            let createdadsPackage = await AdsPackage.create({ ...validatedBody});

            let reports = {
                "action":"Create ads Package",
            };
            let report = await Report.create({...reports, user: user });
            
            res.status(201).send(createdadsPackage);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { adsPackageId } = req.params;
            await checkExist(adsPackageId, AdsPackage, { deleted: false });
            let adsPackages = await AdsPackage.findById(adsPackageId);
            res.send(adsPackages);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
            return next(new ApiError(403, ('admin only')));

            let { adsPackageId } = req.params;
            await checkExist(adsPackageId, AdsPackage, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedadsPackage = await AdsPackage.findByIdAndUpdate(adsPackageId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update adsPackage",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedadsPackage);
        }
        catch (err) {
            next(err);
        }
    },
    
    async delete(req, res, next) {
        try {
            let user = req.user;
            
            let { adsPackageId } = req.params;
            let adsPackages = await checkExistThenGet(adsPackageId, AdsPackage, { deleted: false });
            
            adsPackages.deleted = true;
            await adsPackages.save();
            let reports = {
                "action":"Delete ads Package",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    async beSmallest(req, res, next) {
        try {
            let { adsPackageId } = req.params;
            let allPackages = await AdsPackage.find({deleted:false});
            for (let id of allPackages ) {
                if(id.type == "LOW" ){
                    id.type = "MID";
                }
                await id.save();
            }
            let adsPackages = await checkExistThenGet(adsPackageId, AdsPackage, { deleted: false });
            
            adsPackages.type = "LOW"
            await adsPackages.save();
           
            res.status(204).send();

        }
        catch (err) {
            next(err);
        }
    },
    async beHight(req, res, next) {
        try {
            let { adsPackageId } = req.params;
            let allPackages = await AdsPackage.find({deleted:false});
            for (let id of allPackages ) {
                if(id.type == "HIGH" ){
                    id.type = "MID";
                }
                await id.save();
            }
            let adsPackages = await checkExistThenGet(adsPackageId, AdsPackage, { deleted: false });
            
            adsPackages.type = "HIGH"
            await adsPackages.save();
           
            res.status(204).send();

        }
        catch (err) {
            next(err);
        }
    },
    async beDafault(req, res, next) {
        try {
            let user = req.user;
            
            let { adsPackageId } = req.params;
            let allPackages = await AdsPackage.find({deleted:false});
            for (let id of allPackages ) {
                id.defaultPackage = false;
                await id.save();
            }
            let adsPackages = await checkExistThenGet(adsPackageId, AdsPackage, { deleted: false });
            adsPackages.defaultPackage = true;
            await adsPackages.save();
            let reports = {
                "action":"determine default package",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send();

        }
        catch (err) {
            next(err);
        }
    },
};